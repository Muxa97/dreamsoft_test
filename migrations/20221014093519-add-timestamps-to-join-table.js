'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('user_room', 'created_at', Sequelize.DataTypes.DATE)
    return queryInterface.addColumn('user_room', 'updated_at', Sequelize.DataTypes.DATE)
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn('user_room', 'created_at')
    return queryInterface.removeColumn('user_room', 'updated_at')
  }
};
