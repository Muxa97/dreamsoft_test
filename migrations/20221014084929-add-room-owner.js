'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.addColumn('rooms', 'owner', {
      type: Sequelize.DataTypes.INTEGER,
      allowNull: false
    })
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.removeColumn('rooms', 'owner')
  }
};
