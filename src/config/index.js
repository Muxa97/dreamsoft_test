
const PORT = process.env.PORT || 3000

const DB_HOST = process.env.DB_HOST || 'localhost'
const DB_PORT = process.env.DB_PORT || 3306
const DB_USER = process.env.DB_USER || 'root'
const DB_PASS = process.env.DB_PASS || 'root'
const DB_NAME = process.env.DB_NAME || 'dreamsoft_test'

const SECRET_TOKEN = process.env.SECRET_TOKEN || 'abcdefgh'

const SOCKET_PORT = process.env.SOCKET_PORT || 3001

module.exports = {
    PORT,
    DB_HOST,
    DB_PORT,
    DB_USER,
    DB_PASS,
    DB_NAME,
    SECRET_TOKEN,
    SOCKET_PORT
}