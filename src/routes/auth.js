const express = require('express')
const jwt = require('jsonwebtoken')
const db = require('../database')
const { SECRET_TOKEN } = require('../config')

const router = express.Router()

router.post('/register', async (req, res) => {
    const { login, password } = req.body

    try {
        if (!login || !password) {
            throw new Error('Invalid params.')
        }

        const created = await db.models.User.create({
            login,
            password
        })        

        res.cookie('token', jwt.sign({ id: created.id }, SECRET_TOKEN))

        res.status(200).send({
            id: created.id,
            login: created.login
        })
    } catch (err) {
        console.error(err)
        res.status(500).send(err)
    }
})

router.post('/', async (req, res) => {
    const { login, password } = req.body

    try {
        if (!login || !password) {
            throw new Error('Invalid params.')
        }

        const user = await db.models.User.findOne({ 
            where: { login }
        })

        if (!user) {
            throw new Error('User not found.')
        }

        if (!user.checkPassword(password)) {
            throw new Error('Invalid password.')
        }

        res.cookie('token', jwt.sign({ id: user.id }, SECRET_TOKEN))

        res.status(200).send({
            id: user.id,
            login: user.login
        })
    } catch (err) {
        console.error(err)
        res.status(500).send(err)
    }
})

module.exports = router