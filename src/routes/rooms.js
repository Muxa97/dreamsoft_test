const express = require('express')
const { Op } = require('sequelize')
const db = require('../database')
const checkAuth = require('../middleware/checkAuth')

const io = require('../socketServer')

const router = express.Router()

router.use(checkAuth)

router.post('/new', async (req, res) => {
    try {
        const link = (Math.random() + 1).toString(36).substring(5)

        const room = await db.models.Room.create({
            joinLink: link,
            isPrivate: req.body.isPrivate,
            owner: req.user.id
        })
        await room.addUser(req.user)
        await req.user.addRoom(room)

        if (!room.isPrivate) {
            io.emit('room new', { room: room.joinLink, roomId: room.id })
        }

        res.status(200).send({
            id: room.id,
            link: room.joinLink
        })
    } catch (err) {
        console.error(err)
        res.status(500).send(err)
    }
})

router.get('/join/:link', async (req, res) => {
    try {
        const { link } = req.params
        const room = await db.models.Room.findOne({
            where: {
                joinLink: link
            }
        })

        if (!room) {
            throw new Error('Room with this link not found.')
        }

        const usersCount = await room.countUsers({
            where: {
                id: req.user.id
            }
        })
        if (usersCount) {
            throw new Error('You already joined this room.')
        }

        await room.addUser(req.user)
        await req.user.addRoom(room)

        io.to(link).emit('user join', {
            login: req.user.login
        })

        io.sockets.sockets.forEach((socket, socketId) => {
            if (socket.user.login === req.user.login) {
                socket.join(link)
            }
        })

        res.status(200).send('OK')
    } catch (err) {
        console.error(err)
        res.status(500).send(err)
    }
})

router.get('/leave/:link', async (req, res) => {
    try {
        const { link } = req.params
        const room = await db.models.Room.findOne({
            where: { joinLink: link }
        })

        if (!room) {
            throw new Error('Room does not exist.')
        }

        const usersCount = await room.countUsers({
            where: { id: req.user.id }
        })

        if (!usersCount) {
            throw new Error('You are not a member.')
        }

        await room.removeUser(req.user)
        await req.user.removeRoom(room)

        io.to(link).emit('user leave', {
            login: req.user.login,
            room: link
        })

        res.status(200).send('OK')
    } catch (err) {
        console.error(err)
        res.status(500).send(err)
    }
})

router.post('/remove', async (req, res) => {
    try {
        const { roomId } = req.body
        const room = await db.models.Room.findByPk(roomId)

        if (!room) {
            throw new Error('Room does not exist.')
        }

        if (room.owner !== req.user.id) {
            throw new Error('Only owner can delete room.')
        }

        await req.user.removeRoom(room)
        await db.models.Room.destroy({ where: { id: room.id } })

        io.to(room.joinLink).emit('room remove', { room: room.joinLink })

        res.status(200).send('OK')
    } catch (err) {
        console.error(err)
        res.status(500).send(err)
    }
})

router.get('/:link/messages', async (req, res) => {
    try {
        const { link } = req.params
        const { limit, offset } = req.query

        const room = await db.models.Room.findOne({ 
            where: { joinLink: link },
        })

        if (!room) {
            throw new Error('Room does not exist.')
        }

        const messages = await db.models.Message.findAll({
            where: { roomId: room.id },
            order: [ [ 'created_at', 'DESC' ] ],
            limit: limit || 10,
            offset: offset || 0,
            include: [ db.models.User ]
        })

        res.status(200).send(messages)
    } catch (err) {
        console.error(err)
        res.status(500).send(err)
    }
})

router.post('/:link/sendMsg', async (req, res) => {
    try {
        const { link } = req.params
        const { text } = req.body

        if (!text) {
            throw new Error('Invalid params.')
        }

        const room = await db.models.Room.findOne({ where: { joinLink: link }})
        if (!room) {
            throw new Error('Room does not exist.')
        }

        const message = await db.models.Message.create({
            text,
            roomId: room.id,
            userId: req.user.id
        })

        room.addMessage(message)

        io.to(link).emit('message new', { 
            ...message.dataValues,
            User: { id: req.user.id, login: req.user.login }
          })

        res.status(200).send({ 
            ...message.dataValues, 
            User: { id: req.user.id, login: req.user.login } 
        })
    } catch (err) {
        console.error(err)
        res.status(500).send(err)
    }
})

router.post('/:link/deleteMsg', async (req, res) => {
    try {
        const { link } = req.params
        const { messageId } = req.body

        if (!messageId) {
            throw new Error('Invalid params.')
        }

        const room = await db.models.Room.findOne({
            where: { joinLink: link },
            include: [{
                model: db.models.Message,
                where: {
                    id: messageId
                }
            }]
        })

        const msg = room.Message

        if (!msg) {
            throw new Error('Message does not exist.')
        }

        if (msg.userId !== req.user.id) {
            throw new Error('Only author can delete message.')
        }

        room.removeMessage(msg)
        db.models.Message.destroy(msg)

        io.to(link).emit('message remove', {
            messageId: message.id,
            room: link
        })

        res.status(200).send('OK')
    } catch (err) {
        console.error(err)
        res.status(500).send(err)
    }
})

router.get('/', async (req, res) => {
    try {
        const userRooms = await req.user.getRooms({
            include: [ {
                model: db.models.User,
                attributes: [ 'id', 'login' ]
            }]
        })

        const rooms = await db.models.Room.findAll({
            where: { isPrivate: false, owner: { [Op.ne]: req.user.id } },
            include: [ {
                model: db.models.User,
                attributes: [ 'id', 'login' ]
            }]
        })

        res.status(200).send([ ...userRooms, ...rooms ])
    } catch (err) {
        console.error(err)
        res.status(500).send(err)
    }
})

module.exports = router