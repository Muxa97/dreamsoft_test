const express = require('express')
const router = express.Router()

const auth = require('./auth')
const rooms = require('./rooms')

router.use('/auth', auth)
router.use('/rooms', rooms)

module.exports = router