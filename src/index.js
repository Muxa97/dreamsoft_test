const express = require('express')
const cookieParser = require('cookie-parser')
const { PORT, SOCKET_PORT } = require('./config')
const router = require('./routes')
const io = require('./socketServer')

const app = express()

app.use(express.json())
app.use(cookieParser())



app.get('/', (req, res) => {
    res.sendFile(__dirname + '/test.html')
})
app.use('/', router)

//Init database
const db = require('./database')
db.checkDatabase()
    .then(() => db.init())
    .then(() => console.log('Database initialized'))
    .then(() => console.log(db.models))
    .catch(err => console.error(err))


app.listen(PORT, () => {
    console.log(`App listening on localhost:${PORT}`)
})

io.listen(SOCKET_PORT)