const { Model, DataTypes } = require('sequelize')

class Room extends Model {}

module.exports = (sequelize) => {
    Room.init({
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        joinLink: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        isPrivate: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        owner: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    }, {
        sequelize,
        timestamps: true,
        underscored: true
    })

    return Room
}