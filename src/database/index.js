const { Sequelize } = require('sequelize')
const mysql = require('mysql2/promise')

const { DB_HOST, DB_PORT, DB_USER, DB_PASS, DB_NAME } = require('../config')

class Database {
    constructor() {
        this.sequelize = null
        this.models = {}
    }

    async checkDatabase() {
        try {
            const connection = await mysql.createConnection({ 
                host: DB_HOST,
                port: DB_PORT,
                user: DB_USER,
                password: DB_PASS
            })
            await connection.query(`CREATE DATABASE IF NOT EXISTS \`${DB_NAME}\`;`)
        } catch (err) {
            console.error(`Database connection error: ${err.message}`)
        }
    }

    async init() {
        try {
            const sequelize = new Sequelize(`mysql://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}`)
            await sequelize.authenticate()

            this.sequelize = sequelize

            // Initialize models
            const User = require('./models/user.model')(sequelize)
            const Room = require('./models/room.model')(sequelize)
            const Message = require('./models/message.model')(sequelize)

            User.belongsToMany(Room, { through: 'user_room' })
            Room.belongsToMany(User, { through: 'user_room' })

            User.hasMany(Room, { foreignKey: 'owner' })
            Room.belongsTo(User, { foreignKey: 'owner' })

            User.hasMany(Message, {
                foreignKey: 'user_id'
            })
            Message.belongsTo(User, {
                foreignKey: 'user_id'
            })

            Room.hasMany(Message, {
                foreignKey: 'room_id'
            })
            Message.belongsTo(Room, {
                foreignKey: 'room_id'
            })

            this.models.User = User
            this.models.Room = Room
            this.models.Message = Message
        } catch (err) {
            console.error(`Database error: ${err.message}`)
        }
    }
}

module.exports = new Database()