const jwt = require("jsonwebtoken")
const db = require('../database')
const { SECRET_TOKEN } = require('../config')

const checkAuth = async (req, res, next) => {
    if (req.cookies.token) {
        jwt.verify(
            req.cookies.token,
            SECRET_TOKEN,
            async (err, payload) => {
                if (err) {
                    res.status(401).send(err.message)
                    return
                }

                const user = await db.models.User.findOne({
                    where: { id: payload.id }
                })

                if (!user) {
                    res.status(500).send('User not found.')
                    return
                }

                req.user = user
                next()
            }
        )
    }
}

module.exports = checkAuth