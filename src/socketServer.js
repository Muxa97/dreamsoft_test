const { Server } = require('socket.io')
const db = require('./database')

const io = new Server({ cors: { origin: 'http://localhost:3000' } })

io.use((socket, next) => {
    const userId = socket.handshake.auth.userId
    if (!userId) {
        return next(new Error('Invalid auth params.'))
    }

    socket.userId = userId
    next()
})

io.on('connection', async (socket) => {
    const user = await db.models.User.findByPk(socket.userId, {
        include: [{
            model: db.models.Room,
            attributes: [ 'joinLink' ]
        }]
    })

    if (!user) {
        throw new Error('User not found.')
    }
    socket.user = user

    socket.join(user.Rooms.map(room => room.joinLink))

    socket.on('user leave', ({ login, room }) => {
        if (socket.user.login === login) {
            socket.leave(room)
        }
    })

    socket.on('room remove', ({ room }) => {
        socket.leave(room)
    })
})

module.exports = io